/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.zongtui.web.modules.sys.dao;

import com.zongtui.web.common.persistence.TreeDao;
import com.zongtui.web.common.persistence.annotation.MyBatisDao;
import com.zongtui.web.modules.sys.entity.Area;

/**
 * 区域DAO接口
 * @author ThinkGem
 * @version 2014-05-16
 */
@MyBatisDao
public interface AreaDao extends TreeDao<Area> {
	
}
