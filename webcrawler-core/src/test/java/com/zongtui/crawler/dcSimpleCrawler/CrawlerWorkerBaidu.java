package com.zongtui.crawler.dcSimpleCrawler;


import com.zongtui.fourinone.file.WareHouse;
import com.zongtui.fourinone.worker.MigrantWorker;

public class CrawlerWorkerBaidu extends MigrantWorker
{	
	public WareHouse doTask(WareHouse inhouse)
	{
		String word = inhouse.getString("word");
		System.out.println(word+" 得到控制命令，开始爬取…….");
		
		return new WareHouse("word", word+" world!");
	}
	
	public static void main(String[] args)
	{
		CrawlerWorkerBaidu mw = new CrawlerWorkerBaidu();
		mw.waitWorking("CrawlerWorker");
	}
}